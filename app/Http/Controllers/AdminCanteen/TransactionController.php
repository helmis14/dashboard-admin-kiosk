<?php

namespace App\Http\Controllers\AdminCanteen;

use App\Http\Controllers\Controller;
use App\Models\Food;
use App\Models\Student;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Http;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class TransactionController extends Controller
{
    public function index()
    {
        // $productTransactions = ProductTransaction::where('user_id', auth()->user()->id)->where('status','0')->get();

        $token = session()->get('tokenlogin');
        $id_kantin = session()->get('idkantin');

        $response = Http::withHeaders([
            'Authorization' => 'JWT ' . $token['access_token'],
        ])->get('https://api-ekiosk.bisaai.id:8081/ekiosk_dev/kantin/list_produk?is_deleted=0&id_kantin=' . $id_kantin);
        $products =  json_decode($response->body(), true)['data'];

        $response = Http::withHeaders([
            'Authorization' => 'JWT ' . $token['access_token'],
        ])->get('https://api-ekiosk.bisaai.id:8081/ekiosk_dev/admin/get_mahasiswa?is_deleted=0');
        $customers = json_decode($response->body(), true)['data'];

        // $products = Food::where('is_deleted', '0')->where('canteen_id', 1)->get();
        // $customers = Student::where('is_deleted', '0')->get();
        return view('admin_canteen.transaction.index', compact('products', 'customers'));
    }
    public function indexs(Request $request)
    {

        // $token = session()->get('tokenlogin');
        // $response = Http::withHeaders([
        //     'Authorization' => 'JWT ' . $token['access_token'],
        // ])->get('https://api-ekiosk.bisaai.id:8081/ekiosk_dev/kantin/list_produk?is_deleted=0&id_produk=' . $request->id_produk);
        // if ($response->status() == 200) {
        //     $product =  json_decode($response->body(), true)['data'][0];
        // } else {
        //     $product = [];
        // }
        $product = [];
        // $productTransactions = TransactionDetail::where('is_deleted', '0')->where('status', '0')->with('food')->get() ?? [];
        return response()->json([
            'message' => 'success',
            'data' => $product
        ]);
    }
    public function show($id)
    {
        $transaction = Transaction::find($id);
        $transactionDetails = TransactionDetail::where('transaction_id', $transaction->id)->where('is_deleted', '0')->get();
        return view('admin_canteen.transaction.show', compact('transaction', 'transactionDetails'));
    }
    public function store(Request $request)
    {
        // DB::beginTransaction();

        // try {
        //     $transaction = Transaction::create([
        //         'user_id' => auth()->user()->id,
        //         'transaction_code' => $request->transaction_code,
        //         'pay' => $request->pay,
        //     ]);
        //     for ($i = 0; $i < count($request->product); $i++) {
        //         TransactionDetail::create([
        //             'transaction_id' => $transaction->id,
        //             'food_id' => $request->food_id[$i],
        //             'quantity' => $request->quantity[$i]
        //         ]);
        //     }
        //     DB::commit();
        //     return redirect()->back()->with('success', 'Berhasil menambah transaksi');
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return redirect()->back()->with('alert', 'Gagal melakukan transaksi');
        // }
    }
    public function delete(Request $request)
    {
        $transaction = Transaction::find($request->id);
        $transaction->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data transaksi');
    }
    public function update(Request $request)
    {
        $transaction = Transaction::find($request->id);
        $transaction->update($request->all());
        return redirect()->back()->with('success', 'Berhasil mengubah data transaksi');
    }

    public function getProductCode(Request $request)
    {
        $token = session()->get('tokenlogin');
        $response = Http::withHeaders([
            'Authorization' => 'JWT ' . $token['access_token'],
        ])->get('https://api-ekiosk.bisaai.id:8081/ekiosk_dev/kantin/list_produk?is_deleted=0&id_produk=' . $request->search);
        if ($response->status() == 200) {
            $product =  json_decode($response->body(), true)['data'][0];
        } else {
            $product = '';
        }
        // $product = Food::find($request->search) ?? '';
        return response()->json([
            'message' => 'success',
            'data' => $product
        ]);
    }
    public function addToCart(Request $request)
    {
        $product = Food::find($request->product_code);
        DB::beginTransaction();
        try {
            $cek = TransactionDetail::where('is_deleted', '0')
                ->where('status', '0')->where('food_id', $product->id);
            if ($cek->first()) {
                $quantity = $cek->first()->quantity + $request->quantity;
                $cek->update([
                    'quantity' => $quantity,
                    'price' => $product->price,
                    'total_price' => $product->price * $quantity,
                ]);
                $productTransaction = $cek->with('food')->first();
            } else {
                $productTransaction = new TransactionDetail();
                $productTransaction->food_id = $product->id;
                $productTransaction->quantity = $request->quantity;
                $productTransaction->price = $product->price;
                $productTransaction->total_price = $product->price * $request->quantity;
                $productTransaction->status = '0';
                $productTransaction->save();
                $productTransaction = TransactionDetail::where('id', $productTransaction->id)->with('food')->first();
            }

            DB::commit();
            return response()->json([
                'message' => 'success',
                'data' => $productTransaction
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'failed',
                'data' => $e->getMessage(),
            ], 500);
        }
    }
    public function deleteCart(Request $request)
    {
        $cart = TransactionDetail::find($request->id);
        $cart->delete();
        return response()->json([
            'message' => 'success',
            'data' => $cart
        ], 200);
    }
    public function totalBuy()
    {
        // $token = session()->get('tokenlogin');
        // $response = Http::withHeaders([
        //     'Authorization' => 'JWT ' . $token['access_token'],
        // ])->get('https://api-ekiosk.bisaai.id:8081/ekiosk_dev/kantin/list_produk?is_deleted=0');
        // $productTransactions = json_decode($response->body(), true)['data'];

        $productTransactions = TransactionDetail::where('is_deleted', '0')->where('status', '0')->get() ?? [];
        $total = [];
        foreach ($productTransactions as $product) {
            $total[] = $product['harga'] * $product->quantity;
        }
        $totalBuy = array_sum($total);
        return response()->json([
            'message' => 'success',
            'data' => $totalBuy
        ]);
    }
    public function pay(Request $request)
    {
        DB::beginTransaction();
        try {
            $transactionDetail = TransactionDetail::where('is_deleted', '0')->where('status', '0');
            if (count($transactionDetail->get())) {
                $purchaseOrder = [];
                foreach ($transactionDetail->get() as $product) {
                    $purchaseOrder[] = $product->total_price;
                }
                $totalPurchase = array_sum($purchaseOrder);
                $random = Str::random(10);

                $transaction = new Transaction;
                $transaction->canteen_id = 1;
                $transaction->student_id = $request->student_id;
                $transaction->grand_total = $totalPurchase;
                $transaction->transaction_code = $random;
                $transaction->pay = $request->payment;
                $transaction->return = $request->return;
                $transaction->status = 'done';
                $transaction->save();
                $transactionDetail->update([
                    'transaction_id' => $transaction->id,
                    'status' => '1',
                ]);
                DB::commit();
            }
            return redirect()->route('admin_canteen.transaction.show', $transaction->id)->with('success', 'Pembayaran berhasil');
        } catch (\Exception $e) {
            DB::rollBack();
            $var = response()->json([
                'message' => 'failed',
                'data' => $e
            ], 500);
            return redirect()->back()->with('alert', $e->getMessage());
        }
    }
    public function print($id)
    {
        $transaction = Transaction::find($id);
        $transactionDetails = TransactionDetail::where('transaction_id', $transaction->id)->where('is_deleted', '0')->get();
        return view('admin_canteen.transaction.print', compact('transaction', 'transactionDetails'));
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;
    
    protected $guarded = [];
    
    public function food()
    {
        return $this->belongsTo(Food::class);
    }
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}

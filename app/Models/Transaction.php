<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    public function canteen()
    {
        return $this->belongsTo(Canteen::class);
    }
    public function transactionDetail()
    {
        return $this->hasMany(TransactionDetail::class);
    }
}
